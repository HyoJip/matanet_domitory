package com.metanet.project.model;

import java.time.LocalDate;

public class AdminListVO {
	private String domitoryName;
	private String domitorySection;
	private int studentYear;
	private String userId;
	private String name;
	private LocalDate applyDate;
	private String applyStatus;
	private String PaymentStatus;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDomitoryName() {
		return domitoryName;
	}
	public void setDomitoryName(String domitoryName) {
		this.domitoryName = domitoryName;
	}
	public String getDomitorySection() {
		return domitorySection;
	}
	public void setDomitorySection(String domitorySection) {
		this.domitorySection = domitorySection;
	}
	public int getStudentYear() {
		return studentYear;
	}
	public void setStudentYear(int studentYear) {
		this.studentYear = studentYear;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public LocalDate getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(LocalDate applyDate) {
		this.applyDate = applyDate;
	}
	public String getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}
	public String getPaymentStatus() {
		return PaymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		PaymentStatus = paymentStatus;
	}
	
}
