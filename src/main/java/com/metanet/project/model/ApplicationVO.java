package com.metanet.project.model;

import java.time.LocalDate;

public class ApplicationVO {

	private final Long no;
	private final LocalDate applyDate;
	private String refundBank;
	private String refundAccount;
	private String depositor;
	private String applyStatus;
	private String paymentStatus;
	private final LoginVO user;
	private final DomitoryVO domitory;

	public ApplicationVO(Long no, String refundBank, String refundAccount, String depositor, String userId, Long domitoryId) {
		this(no, LocalDate.now(), refundBank, refundAccount, depositor, "N", "N", new LoginVO(userId), new DomitoryVO(domitoryId));
	}
	public ApplicationVO(Long no, LocalDate applyDate, String refundBank, String refundAccount, String depositor,
			String applyStatus, String paymentStatus, LoginVO user, DomitoryVO domitory) {
		this.no = no;
		this.applyDate = applyDate;
		this.refundBank = refundBank;
		this.refundAccount = refundAccount;
		this.depositor = depositor;
		this.applyStatus = applyStatus;
		this.paymentStatus = paymentStatus;
		this.user = user;
		this.domitory = domitory;
	}

	public Long getNo() {
		return no;
	}

	public LocalDate getApplyDate() {
		return applyDate;
	}

	public String getRefundBank() {
		return refundBank;
	}

	public String getRefundAccount() {
		return refundAccount;
	}

	public String getDepositor() {
		return depositor;
	}

	public String getApplyStatus() {
		return applyStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public LoginVO getUser() {
		return user;
	}

	public DomitoryVO getDomitory() {
		return domitory;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((no == null) ? 0 : no.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationVO other = (ApplicationVO) obj;
		if (no == null) {
			if (other.no != null)
				return false;
		} else if (!no.equals(other.no))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Application [no=" + no + ", applyDate=" + applyDate + ", refundBank=" + refundBank + ", refundAccount="
				+ refundAccount + ", depositor=" + depositor + ", applyStatus=" + applyStatus + ", paymentStatus="
				+ paymentStatus + ", user=" + user + ", domitory=" + domitory + "]";
	}

	static public class Builder {
		private Long no;
		private LocalDate applyDate;
		private String refundBank;
		private String refundAccount;
		private String depositor;
		private String applyStatus;
		private String paymentStatus;
		private LoginVO user;
		private DomitoryVO domitory;
		
		public Builder() {}
		
		public Builder(ApplicationVO application) {
			this.no = application.no;
			this.applyDate = application.applyDate;
			this.refundBank = application.refundBank;
			this.refundAccount = application.refundAccount;
			this.depositor = application.depositor;
			this.applyStatus = application.applyStatus;
			this.paymentStatus = application.paymentStatus;
			this.user = application.user;
			this.domitory = application.domitory;
		}
		
		public Builder no(Long no) {
			this.no = no;
			return this;
		}
		
		public Builder applyDate(LocalDate applyDate) {
			this.applyDate = applyDate;
			return this;
		}
		
		public Builder refundBank(String refundBank) {
			this.refundBank = refundBank;
			return this;
		}
		
		public Builder refundAccount(String refundAccount) {
			this.refundAccount = refundAccount;
			return this;
		}
		
		public Builder depositor(String depositor) {
			this.depositor = depositor;
			return this;
		}
		
		public Builder applyStatus(String applyStatus) {
			this.applyStatus = applyStatus;
			return this;
		}
		
		public Builder paymentStatus(String paymentStatus) {
			this.paymentStatus = paymentStatus;
			return this;
		}
		
		public Builder user(LoginVO user) {
			this.user = user;
			return this;
		}
		
		public Builder domitory(DomitoryVO domitory) {
			this.domitory = domitory;
			return this;
		}
		
		public ApplicationVO build() {
			return new ApplicationVO(no, applyDate, refundBank, refundAccount, depositor, applyStatus, paymentStatus, user, domitory);
		}
	}
}
