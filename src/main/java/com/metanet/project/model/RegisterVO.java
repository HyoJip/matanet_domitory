package com.metanet.project.model;

public class RegisterVO {

	private Long no;
	private float score;
	private SubjectVO subject;
	
	public double getGrade() {
		return convertToGrade() * subject.getCredit();
	}
	
	public String getAlpha() {
		String alpha = "D0";
		
		if (score >= 95) {
			alpha = "A+";
		} else if (score >= 90) {
			alpha = "A0";
		} else if (score >= 85) {
			alpha = "B+";
		} else if (score >= 80) {
			alpha = "B0";
		} else if (score >= 75) {
			alpha = "C+";
		} else if (score >= 70) {
			alpha = "C0";
		} else if (score >= 65) {
			alpha = "D+";
		}
		return alpha;
	}

	private float convertToGrade() {
		float grade = 1.0F;
		
		if (score >= 95) {
			grade = 4.5F;
		} else if (score >= 90) {
			grade = 4.0F;
		} else if (score >= 85) {
			grade = 3.5F;
		} else if (score >= 80) {
			grade = 3.0F;
		} else if (score >= 75) {
			grade = 2.5F;
		} else if (score >= 70) {
			grade = 2.0F;
		} else if (score >= 65) {
			grade = 1.5F;
		}
		return grade;
	}
	
	@Override
	public String toString() {
		return "Register [no=" + no + ", score=" + score + ", subject=" + subject + "]";
	}

	public Long getNo() {
		return no;
	}

	public void setNo(Long no) {
		this.no = no;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public SubjectVO getSubject() {
		return subject;
	}

	public void setSubject(SubjectVO subject) {
		this.subject = subject;
	}

}
