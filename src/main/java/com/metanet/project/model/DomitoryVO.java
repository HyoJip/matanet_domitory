package com.metanet.project.model;

import java.time.LocalDate;

public class DomitoryVO {
	private Long no;
	private String name;
	private String section;
	private int roomType;
	private int cost;
	private int limitNumber;
	private int deposit;
	private LocalDate joinDomitoryDate;
	private LocalDate applyDomitoryDate;
	private LocalDate finishDomitoryDate;

	public DomitoryVO() {
	}

	public DomitoryVO(Long no) {
		this.no = no;
	}

	@Override
	public String toString() {
		return "Domitory [no=" + no + ", name=" + name + ", section=" + section + ", roomType=" + roomType + ", cost="
				+ cost + ", limitNumber=" + limitNumber + ", deposit=" + deposit + ", joinDomitoryDate="
				+ joinDomitoryDate + ", applyDomitoryDate=" + applyDomitoryDate + ", finishDomitoryDate="
				+ finishDomitoryDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((section == null) ? 0 : section.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomitoryVO other = (DomitoryVO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (section == null) {
			if (other.section != null)
				return false;
		} else if (!section.equals(other.section))
			return false;
		return true;
	}

	public Long getNo() {
		return no;
	}

	public void setNo(Long no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public int getRoomType() {
		return roomType;
	}

	public void setRoomType(int roomType) {
		this.roomType = roomType;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getLimitNumber() {
		return limitNumber;
	}

	public void setLimitNumber(int limitNumber) {
		this.limitNumber = limitNumber;
	}

	public int getDeposit() {
		return deposit;
	}

	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}

	public LocalDate getJoinDomitoryDate() {
		return joinDomitoryDate;
	}

	public void setJoinDomitoryDate(LocalDate joinDomitoryDate) {
		this.joinDomitoryDate = joinDomitoryDate;
	}

	public LocalDate getApplyDomitoryDate() {
		return applyDomitoryDate;
	}

	public void setApplyDomitoryDate(LocalDate applyDomitoryDate) {
		this.applyDomitoryDate = applyDomitoryDate;
	}

	public LocalDate getFinishDomitoryDate() {
		return finishDomitoryDate;
	}

	public void setFinishDomitoryDate(LocalDate finishDomitoryDate) {
		this.finishDomitoryDate = finishDomitoryDate;
	}

}
