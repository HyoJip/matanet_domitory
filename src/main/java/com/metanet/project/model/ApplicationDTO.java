package com.metanet.project.model;

import java.time.LocalDate;

public class ApplicationDTO {
	private LocalDate applyDate;
	private String refundBank;
	private String refundAccount;
	private String depositor;
	private String applyStatus;
	private String paymentStatus;
	private String userId;
	private Long domitoryNo;

	public LocalDate getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(LocalDate applyDate) {
		this.applyDate = applyDate;
	}

	public String getRefundBank() {
		return refundBank;
	}

	public void setRefundBank(String refundBank) {
		this.refundBank = refundBank;
	}

	public String getRefundAccount() {
		return refundAccount;
	}

	public void setRefundAccount(String refundAccount) {
		this.refundAccount = refundAccount;
	}

	public String getDepositor() {
		return depositor;
	}

	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}

	public String getApplyStatus() {
		return applyStatus;
	}

	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getDomitoryNo() {
		return domitoryNo;
	}

	public void setDomitoryNo(Long domitoryNo) {
		this.domitoryNo = domitoryNo;
	}

}
