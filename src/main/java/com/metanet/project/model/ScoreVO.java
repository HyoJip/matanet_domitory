package com.metanet.project.model;

import java.time.LocalDate;

public class ScoreVO {
	private LocalDate semesterYear;
	private int semester;
	private int sumCredit;
	private float avgScore;
	public LocalDate getSemesterYear() {
		return semesterYear;
	}
	public void setSemesterYear(LocalDate semesterYear) {
		this.semesterYear = semesterYear;
	}
	public int getSemester() {
		return semester;
	}
	public void setSemester(int semester) {
		this.semester = semester;
	}
	public int getSumCredit() {
		return sumCredit;
	}
	public void setSumCredit(int sumCredit) {
		this.sumCredit = sumCredit;
	}
	public float getAvgScore() {
		return avgScore;
	}
	public void setAvgScore(float avgScore) {
		this.avgScore = avgScore;
	}

}
