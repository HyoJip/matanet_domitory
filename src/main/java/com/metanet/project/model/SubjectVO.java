package com.metanet.project.model;

public class SubjectVO {
	private Long no;
	private String subjectName;
	private int credit;
	private int schoolYear;
	private String subjectTime;
	private String professor;


	@Override
	public String toString() {
		return "Subject [no=" + no + ", subjectName=" + subjectName + ", credit=" + credit + ", schoolYear="
				+ schoolYear + ", subjectTime=" + subjectTime + ", professor=" + professor + "]";
	}

	public Long getNo() {
		return no;
	}

	public void setNo(Long no) {
		this.no = no;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public int getSchoolYear() {
		return schoolYear;
	}

	public void setSchoolYear(int schoolYear) {
		this.schoolYear = schoolYear;
	}

	public String getSubjectTime() {
		return subjectTime;
	}

	public void setSubjectTime(String subjectTime) {
		this.subjectTime = subjectTime;
	}

	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

}
