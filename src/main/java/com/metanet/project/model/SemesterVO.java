package com.metanet.project.model;

import java.time.LocalDate;
import java.util.Collection;

public class SemesterVO implements Comparable<SemesterVO>{

	private Long no;
	private LocalDate year;
	private String semester;
	private Collection<RegisterVO> registers;

	public double getTotalScore() {
		return registers.stream().mapToDouble(RegisterVO::getGrade).sum();
	}
	
	public int getTotalCredit() {
		return registers.stream().mapToInt(register -> register.getSubject().getCredit()).sum();
	}
	
	@Override
	public String toString() {
		return "Semester [no=" + no + ", year=" + year + ", semester=" + semester + ", registers=" + registers + "]";
	}

	public Long getNo() {
		return no;
	}

	public void setNo(Long no) {
		this.no = no;
	}

	public LocalDate getYear() {
		return year;
	}

	public void setYear(LocalDate year) {
		this.year = year;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public Collection<RegisterVO> getRegisters() {
		return registers;
	}

	public void setRegisters(Collection<RegisterVO> registers) {
		this.registers = registers;
	}

	@Override
	public int compareTo(SemesterVO s) {
		if (this.no > s.no) {
			return -1;
		}
		
		return 1;
	}

}
