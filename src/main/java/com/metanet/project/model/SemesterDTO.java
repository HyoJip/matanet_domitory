package com.metanet.project.model;

public class SemesterDTO {

	private Long no;
	private int year;
	private String semester;
	private int totalCredit;
	private double scoreAvg;

	public SemesterDTO(SemesterVO semester) {
		this.no = semester.getNo();
		this.year = semester.getYear().getYear();
		this.semester = semester.getSemester();
		this.totalCredit = semester.getTotalCredit();
		this.scoreAvg = Math.round(semester.getTotalScore() / totalCredit * 100) / 100.0;
	}

	public Long getNo() {
		return no;
	}

	public void setNo(Long no) {
		this.no = no;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public int getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(int totalCredit) {
		this.totalCredit = totalCredit;
	}

	public double getScoreAvg() {
		return scoreAvg;
	}

	public void setScoreAvg(double scoreAvg) {
		this.scoreAvg = scoreAvg;
	}

}
