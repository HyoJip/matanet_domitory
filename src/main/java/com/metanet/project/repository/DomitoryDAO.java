package com.metanet.project.repository;

import java.time.LocalDate;
import java.util.Collection;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.metanet.project.model.ApplicationVO;
import com.metanet.project.model.ApplicationDTO;
import com.metanet.project.model.DomitoryVO;

@Repository
public interface DomitoryDAO {

	ApplicationVO.Builder findApplicationByUserId(@Param("userId") String userId, @Param("joinDate") LocalDate joinDate);

	Collection<DomitoryVO> findAllByJoinDateBefore(@Param("joinDate") LocalDate joinDate);

	Collection<DomitoryVO> getRoomPriceByNameAndSection(@Param("domitoryName") String domitoryName, @Param("domitorySection") String domitorySection);

	int insertApplication(ApplicationDTO applicationDTO);
	
}
