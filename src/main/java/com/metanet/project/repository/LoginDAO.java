package com.metanet.project.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.metanet.project.model.LoginVO;

@Repository
public interface LoginDAO {
	
	public LoginVO login(@Param("userId")String userId, @Param("password")String password);

	public LoginVO getInfo(@Param("userId")String userId);

	public String setInfo(@Param("userId")String userId, @Param("name")String name, @Param("email")String email,
			@Param("address")String address, @Param("phoneNumber")String phoneNumber);

}
