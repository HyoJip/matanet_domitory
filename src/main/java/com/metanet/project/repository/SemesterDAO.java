package com.metanet.project.repository;

import java.util.Collection;

import org.apache.ibatis.annotations.Param;

import com.metanet.project.model.RegisterVO;
import com.metanet.project.model.SemesterVO;

public interface SemesterDAO {

	Long getLatestNoByUserId(@Param("userId") String userId);
	
	SemesterVO findByNo(@Param("semesterNo") Long semesterNo);
	
	Collection<RegisterVO> findByUserIdAndYearAndSemester(@Param("userId") String userId, @Param("semesterYear") String year, @Param("semester") String semester);

	Collection<SemesterVO> findAllScore(String userId);
}
