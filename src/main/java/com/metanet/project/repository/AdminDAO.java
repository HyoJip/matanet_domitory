package com.metanet.project.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.metanet.project.model.AdminListVO;
import com.metanet.project.model.ApplicationDTO;
import com.metanet.project.model.ScoreVO;

@Repository
public interface AdminDAO {

	List<AdminListVO> getList();

	ApplicationDTO getRefundInfo(@Param("userId")String userId);

	ScoreVO getScoreInfo(@Param("userId")String userId, @Param("userId2")String userId2);

	List<AdminListVO> selectCondition(@Param("domitorySection")String domitorySection, 
			@Param("studentYear")String studentYear, @Param("domitoryName")String domitoryName);

	Integer changeApply(@Param("userId")String userId, @Param("applyStatus")String applyStatus);

	Integer changePayment(@Param("userId")String userId, @Param("paymentStatus")String paymentStatus);
}
