package com.metanet.project.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.metanet.project.model.SubjectVO;

@Repository
public interface SubjectDAO {

	List<SubjectVO> selectAll();

	List<SubjectVO> selectRegistered(@Param("userId")String userId, @Param("userId2")String userId2);

	List<SubjectVO> selectProfessorCondition(@Param("condition")String condition);

	List<SubjectVO> selectScoreCondition(@Param("condition")String condition);

	List<SubjectVO> selectYearCondition(@Param("condition")String condition);

	Integer insertSubject(@Param("subjectName")String subjectName, @Param("userId")String userId);

	Integer cancleSubject(@Param("subjectName")String subjectName, @Param("userId")String userId);

}
