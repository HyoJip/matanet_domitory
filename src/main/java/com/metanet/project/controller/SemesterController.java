package com.metanet.project.controller;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.metanet.project.model.RegisterVO;
import com.metanet.project.model.SemesterDTO;
import com.metanet.project.service.SemesterService;

@Controller
public class SemesterController {

	private final SemesterService semesterService;

	public SemesterController(SemesterService semesterService) {
		this.semesterService = semesterService;
	}

	@GetMapping("score_list")
	public String scoreList(HttpSession session, Model model) {
		if (session.getAttribute("userId") == null || session.getAttribute("userId").equals("")) {
			return "redirect:/";
		}
		String userId = session.getAttribute("userId").toString();
		
		List<SemesterDTO> semesters = semesterService.findAllScore(userId)
				.stream().sorted().map(SemesterDTO::new).collect(Collectors.toList());
		List<Integer> years = semesters.stream().map(semester -> semester.getYear()).distinct().collect(Collectors.toList());
		
		model.addAttribute("semesters", semesters);
		model.addAttribute("years", years);

		return "score_list";
	}
	
	@ResponseBody
	@PostMapping("api/semester")
	public Collection<RegisterVO> getRegisters(HttpSession session, @RequestBody Map<String, Object> request) {
		String userId = session.getAttribute("userId").toString();
		
		String year = request.get("semesterYear").toString();
		String semester = request.get("semester").toString();
		return semesterService.searchRegistersBySemester(userId, year, semester);
	}
}
