package com.metanet.project.controller;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.metanet.project.model.SubjectVO;
import com.metanet.project.service.SubjectService;

@Controller
public class SubjectController {
	
	@Autowired
	private SubjectService subjectService;
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/subject_list", method=RequestMethod.POST)
	public @ResponseBody List<SubjectVO> subject_list(){
		List<SubjectVO> list = new ArrayList<>();
		list = subjectService.selectAll();
		return list;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/register_list", method=RequestMethod.POST)
	public @ResponseBody List<SubjectVO> register_list(@RequestBody String data) throws Exception{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String userId = (String)jsonobj.get("userId");
		String userId2 = (String)jsonobj.get("userId");
		List<SubjectVO> list = new ArrayList<>();
		list = subjectService.selectRegistered(userId, userId2);
		return list;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/professor_list", method=RequestMethod.POST)
	public @ResponseBody List<SubjectVO> professor_list(@RequestBody String data) throws Exception{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String condition = (String)jsonobj.get("condition");
		
		List<SubjectVO> list = new ArrayList<>();
		list = subjectService.selectProfessorList(condition);
		
		return list;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/score_list", method=RequestMethod.POST)
	public @ResponseBody List<SubjectVO> score_list(@RequestBody String data) throws Exception{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String condition = (String)jsonobj.get("condition");
		
		List<SubjectVO> list = new ArrayList<>();
		list = subjectService.selectScoreList(condition);
		
		return list;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/year_list", method=RequestMethod.POST)
	public @ResponseBody List<SubjectVO> year_list(@RequestBody String data) throws Exception{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String condition = (String)jsonobj.get("condition");
		
		List<SubjectVO> list = new ArrayList<>();
		list = subjectService.selectYearList(condition);
		
		return list;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/apply_subject", method=RequestMethod.POST)
	public @ResponseBody int apply_subject(@RequestBody String data) throws Exception{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String subjectName = (String)jsonobj.get("subjectName");
		String userId = (String)jsonobj.get("userId");
		int result = subjectService.insertSubject(subjectName, userId);
		return result;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/cancle_subject", method=RequestMethod.POST)
	public @ResponseBody Integer cancle_subject(@RequestBody String data) throws Exception{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String subjectName = (String)jsonobj.get("subjectName");
		String userId = (String)jsonobj.get("userId");
		Integer result = subjectService.cancleSubject(subjectName, userId);
		return result;
	}
}
