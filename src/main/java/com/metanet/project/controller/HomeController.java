package com.metanet.project.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.metanet.project.model.LoginVO;
import com.metanet.project.service.LoginService;



@Controller
public class HomeController {
	
	@Autowired
	private LoginService loginService;
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "login";
	}

	@RequestMapping(value="/loginAction", method=RequestMethod.POST)
	public String loginAction(HttpServletRequest request){
		String userId= request.getParameter("userId");
		String password = request.getParameter("password");
		System.out.println(userId);
		System.out.println(password);
		if(loginService.login(userId, password) == null){
			return "relogin";
		}
		LoginVO loginVO = loginService.login(userId, password);
		HttpSession session = request.getSession(true);
		if(loginVO.getIsStaff() == 2){
			session.setAttribute("userName", "������");
			return "admin";
		}
		session.setAttribute("userId", loginVO.getUserId());
		session.setAttribute("userName", loginVO.getName());
		return "student_info";
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/get_student_info", method=RequestMethod.POST)
	public @ResponseBody LoginVO get_student_info(@RequestBody String data) throws Exception{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String userId = (String)jsonobj.get("userId");
		LoginVO loginVO = loginService.getInfo(userId);
		return loginVO;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/set_student_info", method=RequestMethod.POST)
	public @ResponseBody String set_student_info(@RequestBody String data) throws Exception{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String userId = (String)jsonobj.get("userId");
		String name = (String)jsonobj.get("name");
		String email = (String)jsonobj.get("email");
		String address = (String)jsonobj.get("address");
		String phoneNumber = (String)jsonobj.get("phoneNumber");
		String result = loginService.setInfo(userId, name, email, address, phoneNumber);
		System.out.println(result);
		return result;
	}
		
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin() {
		return "admin";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register() {
		return "register";
	}
	
	@RequestMapping(value = "/relogin", method = RequestMethod.GET)
	public String relogin() {
		return "relogin";
	}
	
	@RequestMapping(value = "/student_info", method = RequestMethod.GET)
	public String student_info() {
		return "student_info";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public String logout(HttpServletRequest request){
		HttpSession session = request.getSession();
        session.invalidate();
        return "login";
	}
	
	@RequestMapping(value = "/domitory/logout", method = RequestMethod.POST)
	public String domitoryLogout(HttpServletRequest request){
		HttpSession session = request.getSession();
        session.invalidate();
        return "redirect:/";
	}
}
