package com.metanet.project.controller;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.metanet.project.model.ApplicationVO;
import com.metanet.project.model.ApplicationDTO;
import com.metanet.project.model.DomitoryVO;
import com.metanet.project.service.DomitoryService;

@Controller
@RequestMapping
public class DomitoryController {

	private final DomitoryService domitoryService;

	public DomitoryController(DomitoryService domitoryService) {
		this.domitoryService = domitoryService;
	}
	
	@GetMapping("/domitory_form")
	public String domitory(HttpSession session, Model model) {
		if (session.getAttribute("userId") == null || session.getAttribute("userId").equals("")) {
			return "redirect:/";
		}
		String userId = session.getAttribute("userId").toString();
		
		Optional<ApplicationVO> optionalApp = domitoryService.findPendingApplicationByUser(userId).map(ApplicationVO.Builder::build);
		if (optionalApp.isPresent()) {
			model.addAttribute("application", optionalApp.get());
			return "application_detail";
		}
		
		Map<String, Object> map = domitoryService.getApplicationForm(userId);
		
		model.addAllAttributes(map);
		
		return "domitory_form";
	}
	
	@GetMapping("domitory/application")
	public String application(HttpSession session, Model model) {
		if (session.getAttribute("userId") == null || session.getAttribute("userId").equals("")) {
			return "redirect:/";
		}
		String userId = session.getAttribute("userId").toString();
		
		Optional<ApplicationVO> optionalApp = domitoryService.findPendingApplicationByUser(userId).map(ApplicationVO.Builder::build);
		if (optionalApp.isPresent()) {
			model.addAttribute("application", optionalApp.get());
			return "application_detail";
		}
		
		return "redirect:/domitory";
	}
	
	@PostMapping("domitory/application")
	public String applyApplication(@ModelAttribute ApplicationDTO appDTO) {
		int isValid = domitoryService.applyForm(appDTO);
		if (isValid == 1) {
			return "redirect:/domitory/application";
		}
		return "domitory_form";
	}
	
	@ResponseBody
	@PostMapping("api/room")
	public Collection<DomitoryVO> approveReserve(@RequestBody Map<String, Object> request) {
		String domitoryName = request.get("domitoryName").toString();
		String domitorySection = request.get("domitorySection").toString();
		return domitoryService.getRoomPrice(domitoryName, domitorySection);
	}
	
}
