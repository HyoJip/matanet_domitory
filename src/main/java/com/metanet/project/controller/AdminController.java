package com.metanet.project.controller;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.metanet.project.model.AdminListVO;
import com.metanet.project.model.ApplicationDTO;
import com.metanet.project.model.ScoreVO;
import com.metanet.project.service.AdminService;

@Controller
public class AdminController {

	@Autowired
	private AdminService adminService;
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/get_domitory_list", method=RequestMethod.POST)
	public @ResponseBody List<AdminListVO> get_domitory_list(){
		List<AdminListVO> adminList = new ArrayList<>();
		adminList = adminService.getList();
		return adminList;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/get_refund_info", method=RequestMethod.POST)
	public @ResponseBody ApplicationDTO get_refund_info(@RequestBody String data) throws ParseException{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String userId = (String)jsonobj.get("userId");
		ApplicationDTO application = adminService.getRefundInfo(userId);
		return application;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/get_score_info", method=RequestMethod.POST)
	public @ResponseBody ScoreVO get_score_info(@RequestBody String data) throws ParseException{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String userId = (String)jsonobj.get("userId");
		String userId2 = (String)jsonobj.get("userId");
		ScoreVO score = adminService.getScoreInfo(userId, userId2);
		return score;
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/selectCondition", method=RequestMethod.POST)
	public @ResponseBody List<AdminListVO> selectCondition(@RequestBody String data) throws ParseException{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String domitorySection = (String)jsonobj.get("domitorySection");
		String studentYear = (String)jsonobj.get("studentYear");
		String domitoryName = (String)jsonobj.get("domitoryName");
		
		List<AdminListVO> list = adminService.selectCondition(domitorySection, studentYear, domitoryName);
		return list;
	}
	
	@CrossOrigin(origins="*")
	@ResponseBody
	@RequestMapping(value="/changeApply", method=RequestMethod.POST)
	public Integer changeApply(@RequestBody String data) throws ParseException{
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONObject jsonobj = (JSONObject) obj;
		String userId = (String)jsonobj.get("userId");
		String applyStatus = (String)jsonobj.get("applyStatus");
		String paymentStatus = (String)jsonobj.get("paymentStatus");
		
		Integer result = adminService.changeApply(userId, applyStatus, paymentStatus);
		
		return result;
	}
}
