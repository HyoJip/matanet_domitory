package com.metanet.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metanet.project.model.SubjectVO;
import com.metanet.project.repository.SubjectDAO;

@Service
public class SubjectService {

	@Autowired
	private SubjectDAO registerDAO;
	
	public List<SubjectVO> selectAll() {
		return registerDAO.selectAll();
	}

	public List<SubjectVO> selectRegistered(String userId, String userId2) {
		return registerDAO.selectRegistered(userId, userId2);
	}

	public List<SubjectVO> selectProfessorList(String condition) {
		return registerDAO.selectProfessorCondition(condition);
	}

	public List<SubjectVO> selectScoreList(String condition) {
		return registerDAO.selectScoreCondition(condition);
	}

	public List<SubjectVO> selectYearList(String condition) {
		return registerDAO.selectYearCondition(condition);
	}

	public Integer insertSubject(String subjectName, String userId) {
		return registerDAO.insertSubject(subjectName, userId);
	}

	public Integer cancleSubject(String subjectName, String userId) {
		return registerDAO.cancleSubject(subjectName, userId);
	}

}
