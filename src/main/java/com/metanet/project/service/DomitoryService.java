package com.metanet.project.service;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.metanet.project.model.ApplicationVO;
import com.metanet.project.model.ApplicationDTO;
import com.metanet.project.model.ApplicationVO.Builder;
import com.metanet.project.model.DomitoryVO;
import com.metanet.project.model.LoginVO;
import com.metanet.project.model.SemesterVO;
import com.metanet.project.repository.DomitoryDAO;
import com.metanet.project.repository.LoginDAO;
import com.metanet.project.repository.SemesterDAO;

@Service
public class DomitoryService {

	private final DomitoryDAO domitoryDAO;
	private final LoginDAO loginDAO;
	private final SemesterDAO semesterDAO;

	public DomitoryService(DomitoryDAO domitoryDAO, LoginDAO loginDAO, SemesterDAO semesterDAO) {
		this.domitoryDAO = domitoryDAO;
		this.loginDAO = loginDAO;
		this.semesterDAO = semesterDAO;
	}

	@Transactional
	public Optional<ApplicationVO.Builder> findPendingApplicationByUser(String userId) {
		Builder applicationBuilder = domitoryDAO.findApplicationByUserId(userId, LocalDate.now());
		return Optional.ofNullable(applicationBuilder);
	}
	
	@Transactional
	public Map<String, Object> getApplicationForm(String userId){
		Map<String, Object> map = new HashMap<>();
		
		Collection<DomitoryVO> allDomitorys = findAllActiveDomitory();
		List<DomitoryVO> domitoryList = allDomitorys.stream().distinct().collect(Collectors.toList());
		LoginVO user = loginDAO.getInfo(userId);
		Long semesterNo = semesterDAO.getLatestNoByUserId(userId);
		if (semesterNo != null) {
			SemesterVO semester = semesterDAO.findByNo(semesterNo);
			int totalCredit = semester.getTotalCredit();
			double avgScore = semester.getTotalScore() / totalCredit;			
			map.put("semester", semester);
			map.put("avgScore", avgScore);
			map.put("totalCredit", totalCredit);
		}
		
		map.put("allDomitorys", allDomitorys);
		map.put("domitoryList", domitoryList);
		map.put("user", user);
		
		return map;
	}
	
	@Transactional
	public int applyForm(ApplicationDTO appDTO) {
		appDTO.setApplyDate(LocalDate.now());
		appDTO.setApplyStatus("N");
		appDTO.setPaymentStatus("N");
		return domitoryDAO.insertApplication(appDTO);
	}
	
	@Transactional
	public Collection<DomitoryVO> findAllActiveDomitory() {
		return domitoryDAO.findAllByJoinDateBefore(LocalDate.now());
	}

	public Collection<DomitoryVO> getRoomPrice(String domitoryName, String domitorySection) {
		return domitoryDAO.getRoomPriceByNameAndSection(domitoryName, domitorySection);
	}
	
}
