package com.metanet.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metanet.project.model.AdminListVO;
import com.metanet.project.model.ApplicationDTO;
import com.metanet.project.model.ScoreVO;
import com.metanet.project.repository.AdminDAO;

@Service
public class AdminService {

	@Autowired
	private AdminDAO adminDAO;
	
	public List<AdminListVO> getList() {
		return adminDAO.getList();
	}

	public ApplicationDTO getRefundInfo(String userId) {
		return adminDAO.getRefundInfo(userId);
	}

	public ScoreVO getScoreInfo(String userId, String userId2) {
		return adminDAO.getScoreInfo(userId, userId2);
	}

	public List<AdminListVO> selectCondition(String domitorySection, String studentYear, String domitoryName) {
		return adminDAO.selectCondition(domitorySection, studentYear, domitoryName);
	}

	public Integer changeApply(String userId, String applyStatus, String paymentStatus) {
		if (paymentStatus != null) {
			return adminDAO.changePayment(userId, paymentStatus);
		}
		return adminDAO.changeApply(userId, applyStatus);
	}

}
