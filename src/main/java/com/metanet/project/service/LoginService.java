package com.metanet.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metanet.project.model.LoginVO;
import com.metanet.project.repository.LoginDAO;

@Service
public class LoginService {

	@Autowired
	private LoginDAO loginDAO;
	
	public LoginVO login(String userId, String password) {
		return loginDAO.login(userId, password);
	}

	public LoginVO getInfo(String userId) {
		return loginDAO.getInfo(userId);
	}

	public String setInfo(String userId, String name, String email, String address, String phoneNumber) {
		return loginDAO.setInfo(userId, name, email, address, phoneNumber);
	}

}
