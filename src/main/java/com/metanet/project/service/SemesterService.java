package com.metanet.project.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.metanet.project.model.RegisterVO;
import com.metanet.project.model.SemesterVO;
import com.metanet.project.repository.SemesterDAO;

@Service
public class SemesterService {
	
	private final SemesterDAO semesterDAO;

	public SemesterService(SemesterDAO semesterDAO) {
		this.semesterDAO = semesterDAO;
	}

	public Collection<SemesterVO> findAllScore(String userId) {
		return semesterDAO.findAllScore(userId);
	}

	public Collection<RegisterVO> searchRegistersBySemester(String userId, String year, String semester) {
		return semesterDAO.findByUserIdAndYearAndSemester(userId, year, semester);
	}

}
