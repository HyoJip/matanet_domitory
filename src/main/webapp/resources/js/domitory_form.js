const DataController = (() => {
	const setInput = (form, name, value) => {
		const myInput = document.createElement("input");
		myInput.setAttribute("name", name);
		myInput.setAttribute("value", value);
		form.appendChild(myInput);
	}
	
	return {
		getRoomCost: async (domitoryName, domitorySection) => {
			return $.ajax({
				url: "/project/api/room",
				type: "POST",
				dataType : "json",
				contentType: "application/json;charset=UTF-8",
				data: JSON.stringify({"domitoryName": domitoryName, "domitorySection": domitorySection}),
				success: async (response) => {
					response.forEach(item => {
						const room = {"type": item.roomType,
									  "cost": item.cost,
									  "deposit": item.deposit,
									  "limitNumber": item.limitNumber,
									  "domitoryNo": item.no};
					});
				}
			});			
		},
		
		applyApplication: (data) => {
			const myForm = document.createElement("form");
			myForm.setAttribute("method", "POST");
			myForm.setAttribute("action", "/project/domitory/application");
			document.body.appendChild(myForm);
			
			setInput(myForm, "refundBank", data.refundBank);
			setInput(myForm, "refundAccount", data.refundAccount);
			setInput(myForm, "depositor", data.depositor);
			setInput(myForm, "userId", data.userId);
			setInput(myForm, "domitoryNo", data.domitoryNo);
			
			myForm.submit();
		},
	}
})();

const UIContoller = (() => {
	const ACTIVE = "active";

	const DOMString = {
		tbody: "tbody",
		index: "#index",
		carousel: ".sections",
		section: "section",
		prevBtn: ".prevBtn",
		nextBtn: ".nextBtn",
		roomList: ".room",
		roomType: ".room_type",
		domitoryName: ".domitory_name",
		domitorySection: ".domitory_section",
		limit: ".limit_number",
		cost: ".price",
		deposit: ".deposit",
		totalPrice: ".total_price",
		refundBank: ".refund_bank",
		refundAccount: ".refund_account",
		depositor: ".depositor",
		userId: ".user_id",
		registerBtn: ".registerBtn"
	};

	const slideCarousel = times => {
		const carousel = document.querySelector(DOMString.carousel);
		const carouselWidth = carousel.offsetWidth;
		carousel.style.transform = `translateX(${-carouselWidth * times}px)`;
		document.querySelector(DOMString.index).scrollIntoView();
	};

	const toggleRoomType = roomTypeNode => {
		document
			.querySelectorAll(DOMString.roomType)
			.forEach(roomType => roomType.classList.remove(ACTIVE));
		roomTypeNode.classList.add(ACTIVE);
	};
	
	const parseRoomPrice = roomPrices => {
		document.querySelector(DOMString.limit).innerText = roomPrices.limitNumber.toLocaleString() + "명";
		document.querySelector(DOMString.cost).innerText = roomPrices.cost.toLocaleString() + "원";
		document.querySelector(DOMString.deposit).innerText = roomPrices.deposit.toLocaleString() + "원";
		document.querySelector(DOMString.totalPrice).innerText = (roomPrices.cost + roomPrices.deposit).toLocaleString() + "원";
	}
	
	const getApplicationData = (domitoryNo) => {
		return {
			"refundBank": $(DOMString.refundBank + " option:selected").val(),
			"refundAccount": $(DOMString.refundAccount).val(),
			"depositor": $(DOMString.depositor).val(),
			"userId": $(DOMString.userId).text(),
			"domitoryNo": domitoryNo,
		};
	}

	return {
		getDOMString: () => DOMString,
		slideCarousel,
		toggleRoomType,
		parseRoomPrice,
		getApplicationData,
	};
})();

const controller = ((UICtrl, DataCtrl) => {
	const DOM = UIContoller.getDOMString();
	const state = {
		carouselIdx: 0,
		roomPrices: [],
		clickedDomitoryNo: 0,
	};

	const setEventListeners = () => {
		document.querySelector(DOM.tbody).addEventListener("click", onClickDomitoryList);
		document
			.querySelectorAll(DOM.prevBtn)
			.forEach(el =>
				el.addEventListener("click", () => UICtrl.slideCarousel(--state.carouselIdx))
			);
		document
			.querySelector(DOM.nextBtn)
			.addEventListener("click", () => UICtrl.slideCarousel(++state.carouselIdx));
		document.querySelector(DOM.roomList).addEventListener("click", onClickRoomList);
		window.addEventListener("resize", () => UICtrl.slideCarousel(state.carouselIdx));
		document.querySelector(DOM.registerBtn).addEventListener("click", onClickRegisterBtn);
	};
	
	const onClickRegisterBtn = () => {
		const data = UICtrl.getApplicationData(state.clickedDomitoryNo);
		DataCtrl.applyApplication(data);
	}

	const onClickDomitoryList = async (event) => {
		const domitoryNode = event.target.closest("tr");
		const domitoryName = domitoryNode.querySelector(DOM.domitoryName).innerText;
		const domitorySection = domitoryNode.querySelector(DOM.domitorySection).innerText;
				
		UICtrl.slideCarousel(++state.carouselIdx);
		
		state["roomPrices"] = await DataCtrl.getRoomCost(domitoryName, domitorySection);
		UICtrl.parseRoomPrice(state.roomPrices[0]);
		state.clickedDomitoryNo = state.roomPrices[0].no;
	};

	const onClickRoomList = event => {
		const roomType = event.target.closest(DOM.roomType);
		if (roomType) {
			UICtrl.toggleRoomType(roomType);
			UICtrl.parseRoomPrice(state.roomPrices[roomType.dataset.type]);
			state.clickedDomitoryNo = state.roomPrices[roomType.dataset.type].no;
		}
	};

	return {
		init: () => {
			setEventListeners();
		},
	};
})(UIContoller, DataController);

controller.init();

$('#apply-page').click(function(){
	location.href="/project/register";
});
$('#student-info').click(function(){
	location.href="/project/student_info";
});
$('#index-page').click(function(){
	location.href="/project/domitory_form";
});
$('#score-page').click(function(){
	location.href="/project/score_list";
});

