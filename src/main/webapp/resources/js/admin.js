$(function(){
	const UIContoller = (() => {
	const ACTIVE = "active";

	const DOMString = {
		tbody: "tbody",
		index: "#index",
		carousel: ".sections",
		section: "section",
		prevBtn: ".prevBtn",
		nextBtn: ".nextBtn",
	};

	const slideCarousel = times => {
		const carousel = document.querySelector(DOMString.carousel);
		const carouselWidth = carousel.offsetWidth;
		carousel.style.transform = `translateX(${-carouselWidth * times}px)`;
		document.querySelector(DOMString.index).scrollIntoView();
	};

	return {
		getDOMString: () => DOMString,
		slideCarousel,
	};
})();

const controller = (UICtrl => {
	const DOM = UIContoller.getDOMString();
	const state = {
		carouselIdx: 0,
	};

	const setEventListeners = () => {
		$(".domitory_list").click(function(){
			UICtrl.slideCarousel(++state.carouselIdx);
			$('#student-name').show();
			$('#select-menu').hide();	
		});
		$(".prevBtn").click(function(){
			UICtrl.slideCarousel(--state.carouselIdx);
			$('#select-menu').show();
			$('#student-name').hide();
		})
		$(".approveBtn").click(function(){
			UICtrl.slideCarousel(--state.carouselIdx);
			$('#select-menu').show();
			$('#student-name').hide();
		})
		$(".returnBtn").click(function(){
			UICtrl.slideCarousel(--state.carouselIdx);
			$('#select-menu').show();
			$('#student-name').hide();
		})
	};
	return {
		init: () => {
			setEventListeners();
		},
	};
})(UIContoller);

controller.init();
})

