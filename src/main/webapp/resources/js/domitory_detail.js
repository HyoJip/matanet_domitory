const price =  Number($(".price").text());
const deposit = Number($(".deposit").text());
const totalPrice = Number($(".total_price").text());
$(".price").text(price.toLocaleString() + "원");
$(".deposit").text(deposit.toLocaleString() + "원");
$(".total_price").text(totalPrice.toLocaleString() + "원");

$('#apply-page').click(function(){
	location.href="/project/register";
});
$('#student-info').click(function(){
	location.href="/project/student_info";
});
$('#index-page').click(function(){
	location.href="/project/domitory_form";
});
$('#score-page').click(function(){
	location.href="/project/score_list";
});

if ($(".paymentBtn")) {
	$(".paymentBtn").click(() => {
		const userId = $(".user_id").text();
		const userData = {
				"userId": userId,
				"paymentStatus": "Y"
		};
		
		$.ajax({
			url:'/project/changeApply',
			type:"POST",
			contentType:"application/json",
			dataType:"JSON",
			data: JSON.stringify(userData),
			success: response => {
				location.href="/project/domitory/application";
			}
		})
	})
}
