$(document).ready(function(){
	
	selectAllList();
	selectRegisteredList();
	
	$("#select-button").click(function(){
		var condition;
	    var radio_btn = document.getElementsByName("condition");
	    for(var i = 0; i<radio_btn.length; i++){
	        if(radio_btn[i].checked==true){
	            condition = radio_btn[i].id;
	            break;
	        }
	    }
	    if($('#input-condition').val() == "" || condition ==null){
	    	alert('조건을 맞게 설정하세요');
	    }else{
		    $('#subject-list-tbody').empty();
		    if(condition=="professor-name"){
		    	selectProfessorList($('#input-condition').val());
		    }
		    else if(condition == "student-year"){
		    	selectYearList($('#input-condition').val());
		    }
		    else if(condition == "score"){
		    	selectScoreList($('#input-condition').val());
		    }
		    
	    }
	});
	$('#student-info').click(function(){
		location.href="student_info";
	});
	$('#index-page').click(function(){
		location.href="domitory_form";
	});
	$('#score-page').click(function(){
		location.href="/project/score_list";
	});
});
function selectAllList(){
	$.ajax({
		url:"http://127.0.0.1:5432/project/subject_list",
		type:"POST",
		success: function(data){
			for(i = 0; i<data.length; i++){
				console.log(data[i]);
				const table = document.getElementById("subject-list").getElementsByTagName('tbody')[0];
				const newRow = table.insertRow();
				
				const newCell1 = newRow.insertCell(0);
				const newCell2 = newRow.insertCell(1);
				const newCell3 = newRow.insertCell(2);
				const newCell4 = newRow.insertCell(3);
				const newCell5 = newRow.insertCell(4);
				const newCell6 = newRow.insertCell(5);
				
				var btn = document.createElement('button');
				btn.innerHTML = "수강";
				btn.id = i;
				btn.value = data[i].subjectName;
				btn.onclick = function(){
					var selected_subjectName = this.value;
					$.ajax({
						url:"http://127.0.0.1:5432/project/register_list",
						type:"POST",
						contentType : "application/json", 
						dataType:"JSON",
						data:JSON.stringify(basicData),
						success: function(data){
							flag = false;
							for(i=0; i<data.length; i++){
								if(data[i].subjectName == selected_subjectName){
									alert('이미 신청되었습니다.');
									flag = true;
								}
							}
							if(flag == false){
								dataSet = {
									"subjectName":selected_subjectName,
									"userId": $('#user-id').val()
								}
								$.ajax({
									url:"http://127.0.0.1:5432/project/apply_subject",
									type:"POST",
									contentType : "application/json", 
									dataType:"JSON",
									data:JSON.stringify(dataSet),
									success: function(){
										$('#registered-list-tbody').empty();
										selectRegisteredList();
									}
								});
								
							}
						}
					});
				};
				newCell1.innerHTML = data[i].subjectName;
				newCell2.innerHTML = data[i].credit;
				newCell3.innerHTML = data[i].schoolYear;
				newCell4.innerHTML = data[i].subjectTime;
				newCell5.innerHTML = data[i].professor;
				newCell6.appendChild(btn);
			} 
		}
	});
}
function selectRegisteredList(){
	basicData ={
			"userId": $('#user-id').val()
	}
	$.ajax({
		url:"http://127.0.0.1:5432/project/register_list",
		type:"POST",
		contentType : "application/json", 
		dataType:"JSON",
		data:JSON.stringify(basicData),
		success: function(data){
			for(i = 0; i<data.length; i++){
				const table = document.getElementById("registered-list").getElementsByTagName('tbody')[0];
				const newRow = table.insertRow();
				
				const newCell1 = newRow.insertCell(0);
				const newCell2 = newRow.insertCell(1);
				const newCell3 = newRow.insertCell(2);
				const newCell4 = newRow.insertCell(3);
				const newCell5 = newRow.insertCell(4);
				const newCell6 = newRow.insertCell(5);
				
				var btn = document.createElement('button');
				btn.innerHTML = "취소";
				btn.value = data[i].subjectName;
				btn.onclick = function(){
					var selected_subjectName = this.value;
					dataSet = {
						"subjectName":selected_subjectName,
						"userId": $('#user-id').val()
					}
					$.ajax({
						url:"http://127.0.0.1:5432/project/cancle_subject",
						type:"POST",
						contentType : "application/json", 
						dataType:"JSON",
						data:JSON.stringify(dataSet),
						success: function(){
							$('#registered-list-tbody').empty();
							selectRegisteredList();
						}
					});
					$('#registered-list-tbody').empty();
					selectRegisteredList();
				};
				newCell1.innerHTML = data[i].subjectName;
				newCell2.innerHTML = data[i].credit;
				newCell3.innerHTML = data[i].schoolYear;
				newCell4.innerHTML = data[i].subjectTime;
				newCell5.innerHTML = data[i].professor;
				newCell6.appendChild(btn);
			}
		}
	});
}
function selectProfessorList(professor){
	dataSet = {
		"condition": professor
	}
	$.ajax({
		url:"http://127.0.0.1:5432/project/professor_list",
		type:"POST",
		contentType : "application/json", 
		dataType:"JSON",
		data: JSON.stringify(dataSet),
		success: function(data){
			for(i = 0; i<data.length; i++){
				const table = document.getElementById("subject-list").getElementsByTagName('tbody')[0];
				const newRow = table.insertRow();
				
				const newCell1 = newRow.insertCell(0);
				const newCell2 = newRow.insertCell(1);
				const newCell3 = newRow.insertCell(2);
				const newCell4 = newRow.insertCell(3);
				const newCell5 = newRow.insertCell(4);
				const newCell6 = newRow.insertCell(5);
				
				var btn = document.createElement('button');
				btn.innerHTML = "수강";
				btn.id = i;
				btn.value = data[i].subjectName;
				btn.onclick = function(){
					var selected_subjectName = this.value;
					$.ajax({
						url:"http://127.0.0.1:5432/project/register_list",
						type:"POST",
						contentType : "application/json", 
						dataType:"JSON",
						data:JSON.stringify(basicData),
						success: function(data){
							flag = false;
							for(i=0; i<data.length; i++){
								if(data[i].subjectName == selected_subjectName){
									alert('이미 신청되었습니다.');
									flag = true;
								}
							}
							if(flag == false){
								dataSet = {
									"subjectName":selected_subjectName,
									"userId": $('#user-id').val()
								}
								$.ajax({
									url:"http://127.0.0.1:5432/project/apply_subject",
									type:"POST",
									contentType : "application/json", 
									dataType:"JSON",
									data:JSON.stringify(dataSet)
								});
								$('#registered-list-tbody').empty();
								selectRegisteredList();
							}
						}
					});
				};
				newCell1.innerHTML = data[i].subjectName;
				newCell2.innerHTML = data[i].credit;
				newCell3.innerHTML = data[i].schoolYear;
				newCell4.innerHTML = data[i].subjectTime;
				newCell5.innerHTML = data[i].professor;
				newCell6.appendChild(btn);
			} 
		}
	});
}
function selectScoreList(score){
	dataSet = {
			"condition": score
		}
		$.ajax({
			url:"http://127.0.0.1:5432/project/score_list",
			type:"POST",
			contentType : "application/json", 
			dataType:"JSON",
			data: JSON.stringify(dataSet),
			success: function(data){
				for(i = 0; i<data.length; i++){
					const table = document.getElementById("subject-list").getElementsByTagName('tbody')[0];
					const newRow = table.insertRow();
					
					const newCell1 = newRow.insertCell(0);
					const newCell2 = newRow.insertCell(1);
					const newCell3 = newRow.insertCell(2);
					const newCell4 = newRow.insertCell(3);
					const newCell5 = newRow.insertCell(4);
					const newCell6 = newRow.insertCell(5);
					
					var btn = document.createElement('button');
					btn.innerHTML = "수강";
					btn.id = i;
					btn.value = data[i].subjectName;
					btn.onclick = function(){
						var selected_subjectName = this.value;
						$.ajax({
							url:"http://127.0.0.1:5432/project/register_list",
							type:"POST",
							contentType : "application/json", 
							dataType:"JSON",
							data:JSON.stringify(basicData),
							success: function(data){
								flag = false;
								for(i=0; i<data.length; i++){
									if(data[i].subjectName == selected_subjectName){
										alert('이미 신청되었습니다.');
										flag = true;
									}
								}
								if(flag == false){
									dataSet = {
										"subjectName":selected_subjectName,
										"userId": $('#user-id').val()
									}
									$.ajax({
										url:"http://127.0.0.1:5432/project/apply_subject",
										type:"POST",
										contentType : "application/json", 
										dataType:"JSON",
										data:JSON.stringify(dataSet)
									});
									$('#registered-list-tbody').empty();
									selectRegisteredList();
								}
							}
						});
					};
					newCell1.innerHTML = data[i].subjectName;
					newCell2.innerHTML = data[i].credit;
					newCell3.innerHTML = data[i].schoolYear;
					newCell4.innerHTML = data[i].subjectTime;
					newCell5.innerHTML = data[i].professor;
					newCell6.appendChild(btn);
				} 
			}
		});
}
function selectYearList(year){
	dataSet = {
			"condition": year
		}
		$.ajax({
			url:"http://127.0.0.1:5432/project/year_list",
			type:"POST",
			contentType : "application/json", 
			dataType:"JSON",
			data: JSON.stringify(dataSet),
			success: function(data){
				for(i = 0; i<data.length; i++){
					const table = document.getElementById("subject-list").getElementsByTagName('tbody')[0];
					const newRow = table.insertRow();
					
					const newCell1 = newRow.insertCell(0);
					const newCell2 = newRow.insertCell(1);
					const newCell3 = newRow.insertCell(2);
					const newCell4 = newRow.insertCell(3);
					const newCell5 = newRow.insertCell(4);
					const newCell6 = newRow.insertCell(5);
					
					var btn = document.createElement('button');
					btn.innerHTML = "수강";
					btn.id = i;
					btn.value = data[i].subjectName;
					btn.onclick = function(){
						var selected_subjectName = this.value;
						$.ajax({
							url:"http://127.0.0.1:5432/project/register_list",
							type:"POST",
							contentType : "application/json", 
							dataType:"JSON",
							data:JSON.stringify(basicData),
							success: function(data){
								flag = false;
								for(i=0; i<data.length; i++){
									if(data[i].subjectName == selected_subjectName){
										alert('이미 신청되었습니다.');
										flag = true;
									}
								}
								if(flag == false){
									dataSet = {
										"subjectName":selected_subjectName,
										"userId": $('#user-id').val()
									}
									$.ajax({
										url:"http://127.0.0.1:5432/project/apply_subject",
										type:"POST",
										contentType : "application/json", 
										dataType:"JSON",
										data:JSON.stringify(dataSet)
									});
									$('#registered-list-tbody').empty();
									selectRegisteredList();
								}
							}
						});
					};
					newCell1.innerHTML = data[i].subjectName;
					newCell2.innerHTML = data[i].credit;
					newCell3.innerHTML = data[i].schoolYear;
					newCell4.innerHTML = data[i].subjectTime;
					newCell5.innerHTML = data[i].professor;
					newCell6.appendChild(btn);
				} 
			}
		});
}
