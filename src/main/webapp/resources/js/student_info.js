$(document).ready(function(){
	basicData ={
			"userId": $('#userId').text()
	}
	printInfo(basicData);
	$('.prevBtn').click(function(){
		updateData ={
			"userId": $('#userId').text(),
			"name": $('#name').val(),
			"email": $('#email').val(),
			"phoneNumber": $('#phone-number').val(),
			"address": $('#address').val(),
		}
		$.ajax({
			url:"http://127.0.0.1:5432/project/set_student_info",
			type:"POST",
			contentType:"application/json",
			dataType:"JSON",
			data: JSON.stringify(updateData),
		})
		alert('수정되었습니다');
		printInfo(basicData);
	});
	$('#apply-page').click(function(){
		location.href="/project/register";
	});
	$('#student-info').click(function(){
		location.href="/project/student_info";
	});
	$('#index-page').click(function(){
		location.href="/project/domitory_form";
	});
	$('#score-page').click(function(){
		location.href="/project/score_list";
	});
})
function printInfo(basicData){
	$.ajax({
		url:"http://127.0.0.1:5432/project/get_student_info",
		type:"POST",
		contentType : "application/json", 
		dataType:"JSON",
		data: JSON.stringify(basicData),
		success: function(data){
			$('#university').text(data.university);
			$('#major').text(data.major);
			$('#student-year').text(data.studentYear);
			$('#register-state').text(data.registerState);
			$('#email').val(data.email);
			$('#phone-number').val(data.phoneNumber);
			$('#address').val(data.address);
		}
	});
}
