$('#apply-page').click(function(){
	location.href="/project/register";
});
$('#student-info').click(function(){
	location.href="/project/student_info";
});
$('#index-page').click(function(){
	location.href="/project/domitory_form";
});
$('#score-page').click(function(){
	location.href="/project/score_list";
});

$(".search_btn").click(() => {
	
	const semesterYear = $(".semester_year option:selected").val();
	const semester = $(".semester_semester option:selected").val();
	
	$.ajax({
		url: "/project/api/semester",
		type: "POST",
		dataType : "json",
		contentType: "application/json;charset=UTF-8",
		data: JSON.stringify({"semesterYear": `${semesterYear}-01-01`, "semester": semester}),
		success: response => {
			const box = document.querySelector(".register_box");
			
			box.innerHTML = "";
			response.forEach(register => {
				const markup = `
							<tr>
								<td>${register.subject.no}</td>
								<td>${register.subject.subjectName}</td>
								<td>${register.subject.credit}</td>
								<td>${register.alpha}</td>
							</tr>`;
				box.insertAdjacentHTML("beforeend", markup);
			});
		}
	});
});

const scoreBox = document.querySelectorAll(".all_register_box tr");

let totalScore = 0;
let totalCredit = 0;

scoreBox.forEach(tr => {
	const credit = tr.querySelector(".total_credit").textContent * 1;
	totalCredit += credit;
	totalScore += credit * tr.querySelector(".avgscore").textContent;
});

$(".total_totalcredit").text(totalCredit);
$(".total_avgscore").text((totalScore/totalCredit).toFixed(2));