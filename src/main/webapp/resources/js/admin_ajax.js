$(document).ready(function(){
	var userId = '';
	$.ajax({
		url:"http://127.0.0.1:5432/project/get_domitory_list",
		type:"POST",
		success: function(data){
			create_list(data);
		}
	});
	$(".domitory_list").click(function(){
		const domitoryId = event.target.closest("tr").id;
		userId = event.target.closest("tr").id;
		basicData ={
			"userId": domitoryId
		}
		$.ajax({
			url:"http://127.0.0.1:5432/project/get_student_info",
			type:"POST",
			contentType : "application/json", 
			dataType:"JSON",
			data: JSON.stringify(basicData),
			success: function(data){
				$('#address').text(data.address);
				$('#email').text(data.email);
				$('#name').text(data.name)
				$('#student-name').text(data.name)
				$('#major').text(data.major);
				$('#phoneNumber').text(data.phoneNumber);
				$('#registerState').text(data.registerState);
				$('#studentYear').text(data.studentYear);
				$('#university').text(data.university);
				$('#userId').text(data.userId);
			}
		});
		$.ajax({
			url:"http://127.0.0.1:5432/project/get_refund_info",
			type:"POST",
			contentType : "application/json", 
			dataType:"JSON",
			data: JSON.stringify(basicData),
			success: function(data){
				$('#depositor').text(data.depositor);
				$('#refundAccount').text(data.refundAccount);
				$('#refundBank').text(data.refundBank);
			}
		});
		$.ajax({
			url:"http://127.0.0.1:5432/project/get_score_info",
			type:"POST",
			contentType : "application/json", 
			dataType:"JSON",
			data: JSON.stringify(basicData),
			success: function(data){
				$('#semesterYear').text(data.semesterYear.year);
				$('#semester').text(data.semester);
				$('#sumCredit').text(data.sumCredit);
				var score = data.avgScore / (22.2222)
				$('#avgScore').text(score.toFixed(2));	
			}
		});
	});
	$(".approveBtn").click(function(){
		alert("승인하였습니다.");
		userData = {
			"userId": userId,
			"applyStatus": "Y"
		}
		$.ajax({
			url:'http://127.0.0.1:5432/project/changeApply',
			type:"POST",
			contentType:"application/json",
			dataType:"JSON",
			data: JSON.stringify(userData),
			success: function(){
				$.ajax({
					url:"http://127.0.0.1:5432/project/get_domitory_list",
					type:"POST",
					success: function(data){
						$('#body-list').empty();
						create_list(data);
					}
				});
			}
		})
	})
	$(".returnBtn").click(function(){
		alert("반려하였습니다.");
		userData = {
			"userId": userId,
			"applyStatus": "N"	
		}
		$.ajax({
			url:'http://127.0.0.1:5432/project/changeApply',
			type:"POST",
			contentType:"application/json",
			dataType:"JSON",
			data: JSON.stringify(userData),
			success:function(){
				$.ajax({
					url:"http://127.0.0.1:5432/project/get_domitory_list",
					type:"POST",
					success: function(data){
						$('#body-list').empty();
						create_list(data);
					}
				});
			}
		})
	})
	$('#select-button').click(function(){
		var domitorySection = $('.domitory-section').val();
		var studentYear = $('.student-year').val();
		var domitoryName = $('.domitory-name').val();
		dataSet ={
			"domitorySection": domitorySection,
			"studentYear": studentYear,
			"domitoryName": domitoryName
		}
		$.ajax({
			url:'http://127.0.0.1:5432/project/selectCondition',
			type:"POST",
			contentType : "application/json", 
			dataType:"JSON",
			data: JSON.stringify(dataSet),
			success: function(data){
				$('#body-list').empty();
				create_list(data);
			}
		});
});
})
function create_list(data){
	for(i=0; i<data.length; i++){
		var date = data[i].applyDate.year+"-2-"+data[i].applyDate.dayOfMonth;
		const table = document.getElementById("apply-list").getElementsByTagName('tbody')[0];
		const newRow = table.insertRow();
		newRow.id = data[i].userId;
		
		const newCell1 = newRow.insertCell(0);
		const newCell2 = newRow.insertCell(1);
		const newCell3 = newRow.insertCell(2);
		const newCell4 = newRow.insertCell(3);
		const newCell5 = newRow.insertCell(4);
		const newCell6 = newRow.insertCell(5);
		const newCell7 = newRow.insertCell(6);
		const newCell8 = newRow.insertCell(7);
		
		newCell1.innerHTML = data[i].domitoryName;
		newCell2.innerHTML = data[i].domitorySection;
		newCell3.innerHTML = data[i].studentYear;
		newCell4.innerHTML = data[i].userId;
		newCell5.innerHTML = data[i].name;
		newCell6.innerHTML = date;
		newCell7.innerHTML = data[i].applyStatus;
		newCell8.innerHTML = data[i].paymentStatus;
	}
}