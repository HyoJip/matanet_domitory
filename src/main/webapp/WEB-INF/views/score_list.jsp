<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>성적정보</title>
    <%@ include file="partials/head.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/score_list.css">
</head>
<body>
    <%@ include file="partials/header.jsp" %>
    <main>
        <%@ include file="partials/nav.jsp" %>
        <article>
            <div class="sections_wrap">
                <section>
                    <h3>학기별 성적조회</h3>
                    <c:choose>
                    	<c:when test="${!empty years}">
		                    <div class="controls">
		                        <label>
		                            <select class="semester_year" type="text">
		                            	<c:forEach var="year" items="${years}">
			                                <option value="${year}">${year}</option>
		                                </c:forEach>
		                            </select>년도
		                        </label>
		                        <label>
		                            <select class="semester_semester" type="text">
		                                <option value="1">1</option>
		                                <option value="2">2</option>
		                            </select>학기
		                        </label>
		                        <button class="search_btn">조회</button>
		                    </div>
		                    <table>
		                        <thead>
		                            <td>과목번호</td>
		                            <td>과목명</td>
		                            <td>학점</td>
		                            <td>성적</td>
		                        </thead>
		                        <tbody class="register_box">
		                        </tbody>
		                    </table>
                    	</c:when>
                    	<c:when test="${empty years}">
                    		<div class="warning_wrap">
	                    		<i class="fas fa-exclamation-triangle"></i>
	                    		<small>성적 정보가 아직 없습니다</small>                    		
                    		</div>
                    	</c:when>
                    </c:choose>
               	</section>
                <c:if test="${!empty semesters}">
	                <section>
	                    <h3>전체 성적조회</h3>
	                    <table>
	                        <thead>
	                            <td>학년도</td>
	                            <td>학기</td>
	                            <td>이수학점</td>
	                            <td>평점평균</td>
	                        </thead>
	                        <tbody class="all_register_box">
	                        <c:forEach var="semester" items="${semesters}">
	                            <tr>
	                                <td>${semester.year}</td>
	                                <td>${semester.semester}학기</td>
	                                <td class="total_credit">${semester.totalCredit}</td>
	                                <td class="avgscore">${semester.scoreAvg}</td>
	                            </tr>
	                           
	                        </c:forEach>
	                        </tbody>
	                        <tfoot>
	                            <td>합계</td>
	                            <td></td>
	                            <td class="total_totalcredit"></td>
	                            <td class="total_avgscore"></td>
	                        </tfoot>
	                    </table>
	                </section>
                </c:if>
            </div>
        </article>
    </main>
    <script src="${pageContext.request.contextPath}/resources/js/score_list.js"></script>
</body>
</html>