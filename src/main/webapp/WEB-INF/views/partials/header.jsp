<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<header>
	<a href="/project/"><img src="${pageContext.request.contextPath}/resources/image/logo03.png" alt="메타넷 대우정보 로고"></a>
    <h1>학사관리시스템</h1>
    <section class="logged_user_box">
        <p class="user_id">${userId}</p>
        <p class="user_name">(${userName})</p>
        <form method="post" action="logout">
        <p><button type="submit" class="logout">로그아웃</button></p>
        </form>
    </section>
</header>
<style>
button {
  padding: 10px 20px;
  background-color: #0c4da2;
  border: 0;
  outline: 0;
  color: white;
  cursor: pointer; }
</style>