<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<nav>
	<ul>
	    <li id ="student-info"><i class="fas fa-user"></i>학생정보</li>
	    <li id="apply-page"><i class="fas fa-university"></i>수업</li>
	    <li id="score-page"><i class="far fa-star"></i>성적</li>
	    <li id="index-page"><i class="fas fa-building"></i>기숙사</a></li>
	</ul>
</nav>
