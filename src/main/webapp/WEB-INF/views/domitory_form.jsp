<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>기숙사 신청</title>
    <%@ include file="partials/head.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/domitory_form.css">
</head>
<body>
	<%@ include file="partials/header.jsp" %>
    <main>
    	<%@ include file="partials/nav.jsp" %>
        <article>
            <p class="index" id="index">📌<a>기숙사</a> > <a>기숙사 선택</a></p>
            <div class="sections_wrap">
                <div class="sections">
                	<section class="domitory_list">
                        <table>
                            <thead>
                                <td>캠퍼스</td>
                                <td>기숙사구분</td>
                                <td>입사시작</td>
                                <td>신청시작</td>
                                <td>신청종료</td>
                            </thead>
                            <tbody>
                            	<c:forEach var="domitory" items="${domitoryList}">
                            		<tr data-id="${domitory.no}">
	                                    <td class="domitory_section">${domitory.section}</td>
	                                    <td class="domitory_name">${domitory.name}</td>
	                                    <td>${domitory.joinDomitoryDate}</td>
	                                    <td>${domitory.applyDomitoryDate}</td>
	                                    <td>${domitory.finishDomitoryDate}</td>
	                                </tr>
                            	</c:forEach>
                            </tbody>
                        </table>
                    </section>
                    <section class="info_wrap">
                        <div class="info_item">
                            <h3>개인정보</h3>
                            <div class="person">
                                <div class="user_info">
                                    <p>성명<span>${user.name}</span></p>
                                    <p>연락처<span>${user.phoneNumber}</span></p>
                                    <p>이메일<span>${user.email}</span></p>
                                    <p>현거주지<span>${user.address}</span></p>
                                </div>
                                <div class="student_info">
                                    <p>학번<span>${user.userId}</span></p>
                                    <p>학년<span>${user.studentYear}</span></p>
                                    <p>대학<span>${user.university}</span></p>
                                    <p>학과<span>${user.major}</span></p>
                                    <p>학적상태<span>${user.registerState}</span></p>
                                </div>
                            </div>
                        </div>
                        <!-- 신입생의 경우 disable처리 -->
                        <div class="info_item">
                            <h3>직전학기 성적</h3>
                            <div class="score">
                                <p>성적취득년도<span>${semester.year}</span></p>
                                <p>취득학기<span>${semester.semester}</span></p>
                                <p>취득학점<span>${totalCredit}</span></p>
                                <p>평점평균<span>${avgScore}</span></p>
                            </div>
                        </div>
                        <div class="info_item">
                            <h3>환불 정보</h3>
                            <div class="refund">
                                <div class="bank">
                                    <label>환불은행
                                        <select type="text" class="refund_bank">
                                            <option value="국민은행">국민은행</option>
                                            <option value="신한은행">신한은행</option>
                                            <option value="카카오뱅크">카카오뱅크</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="account">
                                    <label>환불계좌<input type="text" class="refund_account"></label>
                                    <label>예금주<input type="text" class="depositor"></label>
                                </div>
                            </div>
                        </div>
                        <div class="buttons">
                            <button class="prevBtn">이전</button>
                            <button class="nextBtn">다음</button>
                        </div>
                    </section>
                    <section>
                        <div class="info_item">
                            <h3>기숙사 정보</h3>
                            <div class="domitory">
                                <p>캠퍼스<span>대연캠퍼스</span></p>
                                <p>기숙사종류<span>행복기숙사</span></p>
                                <p>신청학기<span>1학기</span></p>
                                <p>입사일자<span>2021-02-26</span></p>
                                <p>모집자수<span class="limit_number">400</span></p>
                            </div>
                        </div>
                        <div class="info_item">
                            <h3>기숙사 방 종류</h3>
                            <div class="room">
                                <div class="room_type active" data-type="0">
                                    <i class="fas fa-user"></i>
                                    <p>1인실</p>
                                </div>
                                <div class="room_type" data-type="1">
                                    <i class="fas fa-user-friends"></i>
                                    <p>2인실</p>
                                </div>
                                <div class="room_type" data-type="2">
                                    <p><i class="fas fa-user-friends"></i><i class="fas fa-user-friends"></i></p>
                                    <p>4인실</p>
                                </div>
                            </div>
                        </div>
                        <div class="info_item">
                            <h3>기숙사 금액</h3>
                            <div class="cost">
                                <p>기숙사비<span class="price"></span></p>
                                <p>보증금<span class="deposit"></span></p>                
                                <p>합계<span class="total_price"></span></p>                
                            </div>
                        </div>
                        <div class="buttons">
                            <button class="prevBtn">이전</button>
                            <button class="registerBtn">신청</button>
                        </div>
                    </section>
                </div>
            </div>
        </article>
    </main>
    <script src="${pageContext.request.contextPath}/resources/js/domitory_form.js"></script>
</body>
</html>