<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>기숙사 신청 관리</title>
    <%@ include file="partials/head.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/admin.css">
    <script src="${pageContext.request.contextPath}/resources/js/admin.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/admin_ajax.js"></script>
</head>
<body>
    <header>
	<img src="${pageContext.request.contextPath}/resources/image/logo03.png" alt="메타넷 대우정보 로고">
    <h1>학사관리시스템</h1>
    <section class="logged_user_box">
        <p class="user_name">${userName }</p>
        <form method="post" action="logout">
        <p><button type="submit" class="logout">로그아웃</button></p>
        </form>
    </section>
	</header>
    <main>
        <nav>
		<ul>
		    <li><i class="fas fa-building"></i>기숙사 신청현황</a></li>
		</ul>
		</nav>

        <article>
            <p class="index" id="index">📌<a>신청현황</a>  ><a id="student-name"></a></p>
            <div id="select-menu">
                	기숙사구분<select class="domitory-section">
                    <option value="%" selected="selected">모두</option>
                    <option value="신관1">신관1</option>
                    <option value="신관2">신관2</option>
                    <option value="행복기숙사">행복기숙사</option>
                	</select>
                	학년<select class="student-year">
                    <option value="%" selected="selected">모두</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                	</select>
                	신청캠퍼스<select class="domitory-name">
                    <option value="%" selected="selected">모두</option>
                    <option value="대연캠퍼스">대연캠퍼스</option>
                    <option value="용당캠퍼스">용당캠퍼스</option>
                	</select>
                	<button id="select-button">검색</button>
            </div>
            <div class="sections_wrap">
                <div class="sections">
                    <section class="domitory_list">
                        <table id="apply-list">
                            <thead>
                                <td>캠퍼스</td>
                                <td>기숙사구분</td>
                                <td>학년</td>
                                <td>학번</td>
                                <td>이름</td>
                                <td>신청일자</td>
                                <td>승인현황</td>
                                <td>납부현황</td>
                            </thead>
                            <tbody id="body-list">
                            </tbody>
                        </table>
                    </section>
                    <section class="info_wrap">
                        <div class="info_item">
                            <h3>개인정보</h3>
                            <div class="person">
                                <div class="user_info">
                                    <p>성명<span id="name"></span></p>
                                    <p>연락처<span id="phoneNumber"></span></p>
                                    <p>이메일<span id="email"></span></p>
                                    <p>현거주지<span id="address"></span></p>
                                </div>
                                <div class="student_info">
                                    <p>학번<span id="userId"></span></p>
                                    <p>학년<span id="studentYear"></span></p>
                                    <p>대학<span id="university"></span></p>
                                    <p>학과<span id="major"></span></p>
                                    <p>학적상태<span id="registerState"></span></p>
                                </div>
                            </div>
                        </div>
                        <!-- 신입생의 경우 disable처리 -->
                        <div class="info_item">
                            <h3>직전학기 성적</h3>
                            <div class="score">
                                <p>성적취득년도<span id="semesterYear"></span></p>
                                <p>취득학기<span id="semester"></span></p>
                                <p>취득학점<span id="sumCredit"></span></p>
                                <p>평점평균<span id="avgScore"></span></p>
                            </div>
                        </div>
                        <div class="info_item">
                            <h3>환불 정보</h3>
                            <div class="refund">
                                <div class="bank">
                                    <label>환불은행
                                        <span id="refundBank"></span>
                                    </label>
                                </div>
                                <div class="account">
                                    <label>환불계좌<span id="refundAccount"></span></label>
                                    <label>예금주<span id="depositor"></span></label>
                                </div>
                            </div>
                        </div>
                        <div class="buttons">
                            <button class="prevBtn">이전</button>
                            <button class="approveBtn">승인</button>
                            <button class="returnBtn">반려</button>
                        </div>
                    </section>
                </div>
            </div>
        </article>
    </main>
</body>
<style>
button {
  padding: 10px 20px;
  background-color: #0c4da2;
  border: 0;
  outline: 0;
  color: white;
  cursor: pointer; }
</style>
</html>