<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<head>
	<link rel="stylesheet" href='${pageContext.request.contextPath}/resources/css/login.css'>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://kit.fontawesome.com/ad755395c3.js" crossorigin="anonymous"></script>
</head>
	<section class="logo-wrap">
		<img src="${pageContext.request.contextPath}/resources/image/logo.png">
	</section>
	<section class="login-input-section-wrap">
	<form method="post" action="loginAction">
			<div class="login-input-wrap">	
				<input placeholder="학번" type="text" name="userId" id="userId"></input>
			</div>
			<div class="login-input-wrap password-wrap">	
				<input placeholder="비밀번호" type="password" name="password" id="password"></input>
			</div>
			<p style="color:tomato;">회원정보가 틀립니다.</p>
			<div class="login-button-wrap">
				<button type="submit" id="login-button">로그인</button>
			</div>
	</form>
	</section>