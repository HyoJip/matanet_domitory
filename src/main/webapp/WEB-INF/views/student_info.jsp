<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>학생정보</title>
    <%@ include file="partials/head.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/student_info.css">
    <script src="${pageContext.request.contextPath}/resources/js/student_info.js"></script>
</head>
<body>
    <%@ include file="partials/header.jsp" %>
    <main>
        <%@ include file="partials/nav.jsp" %>
        <article>
            <div class="sections_wrap">
                <section class="info_wrap">
                    <h3>학생정보</h3>
                    <div class="view_box">
                        <img src="https://image.flaticon.com/icons/png/512/17/17004.png" alt="회원이미지" class="user_img">
                        <div class="texts">
                            <p>학번<span id="userId">${userId }</span></p>
                            <p>대학<span id="university"></span></p>
                            <p>학과<span id="major"></span></p>
                            <p>학년<span id="student-year"></span></p>
                            <p>학적상태<span id="register-state"></span></p>
                        </div>
                        <div class="inputs">
                            <label>성명<input type="text" value="${userName }" id="name"></label>
                            <label>이메일<input type="text" id="email"></label>
                            <label>연락처<input type="text" id="phone-number"></label>
                            <label>현거주지<input type="text" id="address"></label>
                        </div>
                    </div>
                    <div class="buttons">
                        <button class="prevBtn">수정</button>
                        <button class="nextBtn">취소</button>
                    </div>
                </section>
            </div>
        </article>
    </main>
</body>
</html>