<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>수강신청</title>
	<%@ include file="partials/head.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/register.css">
    <script src="${pageContext.request.contextPath}/resources/js/register.js"></script>
</head>
<body>
	<input type="hidden" id="user-id" value="${userId}" />
    <%@ include file="partials/header.jsp" %>
    <main>
        <%@ include file="partials/nav.jsp" %>
    <div class="section-list">
        <h3 id="select-title">강의조회</h3>
        <div id="select-menu">
            교수명<input type="radio" id ="professor-name" name="condition" />
            학년<input type="radio" id="student-year" name="condition" />
            학점<input type="radio" id="score" name="condition" />
            <input type="text" id="input-condition"/>
            <button id="select-button">검색</button>
        </div>
        <div id="register-list">
            <table id="subject-list">
                <thead>
                    <td>과목명</td>
                    <td>학점</td>
                    <td>학년</td>
                    <td>시간표</td>
                    <td>담당교수</td>
                    <td></td>
                </thead>
                <tbody id="subject-list-tbody">
                </tbody>
            </table>
        </div>
        <h3 id="title">수강신청목록</h3>
        <div class="section-mine">
            <div id="mine-list">
                <table id="registered-list">
                    <thead>
                        <td>과목명</td>
                        <td>학점</td>
                        <td>학년</td>
                        <td>시간표</td>
                        <td>담당교수</td>
                        <td></td>
                    </thead>
                    <tbody id="registered-list-tbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </main>
</body>
</html>