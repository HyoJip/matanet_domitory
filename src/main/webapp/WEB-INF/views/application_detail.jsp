<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>기숙사 신청</title>
    <%@ include file="partials/head.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/application_detail.css">
</head>
<body>
	<%@ include file="partials/header.jsp" %>
    <main>
    	<%@ include file="partials/nav.jsp" %>
        <article>
        	<div class="status">
	            <span class="${application.applyStatus eq 'N'? 'current_status' : ''}">승인대기</span>
	            <i class="fas fa-arrow-right"></i>
	            <c:if test="${application.applyStatus eq 'Y' and (application.paymentStatus eq 'N' or empty application.paymentStatus)}">
           			<span class="current_status">납부대기</span>	            
	            </c:if>
	            <c:if test="${not(application.applyStatus eq 'Y' and (application.paymentStatus eq 'N' or empty application.paymentStatus))}">
           			<span class="">납부대기</span>	            
	            </c:if>
				<i class="fas fa-arrow-right"></i>
				<span class="${application.paymentStatus eq 'Y' ? 'current_status' : ''}">납부확인</span>
			</div>
	            <div class="sections">
	                <section class="info_wrap">
	                    <div class="info_item">
	                        <h3>개인정보</h3>
	                        <div class="person">
	                            <div class="user_info">
	                                <p>성명<span>${application.user.name}</span></p>
	                                <p>연락처<span>${application.user.phoneNumber}</span></p>
	                                <p>이메일<span>${application.user.email}</span></p>
	                                <p>현거주지<span>${application.user.address}</span></p>
	                            </div>
	                            <div class="student_info">
	                                <p>학번<span>${application.user.userId}</span></p>
	                                <p>학년<span>${application.user.studentYear}</span></p>
	                                <p>대학<span>${application.user.university}</span></p>
	                                <p>학과<span>${application.user.major}</span></p>
	                                <p>학적상태<span>${application.user.registerState}</span></p>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- 신입생의 경우 disable처리 -->
	                    <div class="info_item">
	                        <h3>직전학기 성적</h3>
	                        <div class="score">
	                            <p>성적취득년도<span>2020</span></p>
	                            <p>취득학기<span>2</span></p>
	                            <p>취득학점<span>21</span></p>
	                            <p>평점평균<span>3.8</span></p>
	                        </div>
	                    </div>
	                    <div class="info_item">
	                        <h3>환불 정보</h3>
	                        <div class="refund">
	                            <div class="bank">
                                	<p>환불은행<span>${application.refundBank}</span></p>
	                            </div>
	                            <div class="account">
		                            <p>환불계좌<span>${application.refundAccount}</span></p>
		                            <p>예금주<span>${application.depositor}</span></p>
	                            </div>
	                        </div>
	                    </div>
	                </section>
	                <section>
	                    <div class="info_item">
	                        <h3>기숙사 정보</h3>
	                        <div class="domitory">
	                            <p>캠퍼스<span>${application.domitory.name}</span></p>
	                            <p>기숙사종류<span>${application.domitory.section}</span></p>
	                            <p>신청학기<span>1학기</span></p>
	                            <p>입사일자<span>${application.domitory.joinDomitoryDate}</span></p>
	                            <p>모집자수<span>${application.domitory.limitNumber}명</span></p>
	                        </div>
	                    </div>
	                    <div class="info_item">
	                        <h3>기숙사 방 종류</h3>
	                        <div class="room">
	                            <div class="room_type ${application.domitory.roomType == 1? 'active' : ''}" >
	                                <i class="fas fa-user"></i>
	                                <p>1인실</p>
	                            </div>
	                            <div class="room_type ${application.domitory.roomType == 2? 'active' : ''}">
	                                <i class="fas fa-user-friends"></i>
	                                <p>2인실</p>
	                            </div>
	                            <div class="room_type ${application.domitory.roomType == 4? 'active' : ''}">
	                                <p><i class="fas fa-user-friends"></i><i class="fas fa-user-friends"></i></p>
	                                <p>4인실</p>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="info_item">
	                        <h3>기숙사 금액</h3>
	                        <div class="cost">
	                            <p>기숙사비<span class="price">${application.domitory.cost}</span></p>
	                            <p>보증금<span class="deposit">${application.domitory.deposit}</span></p>                
	                            <p>합계<span class="total_price">${application.domitory.cost + application.domitory.deposit}</span></p>                
	                        </div>
	                    </div>
	                    <div class="buttons">
                	    	<c:if test="${application.applyStatus eq 'Y' and (application.paymentStatus eq 'N' or empty application.paymentStatus)}">
	                            <button class="paymentBtn">납부</button>
	           				 </c:if>
                        </div>
	                </section>
	            </div>
        </article>
    </main>
    <script src="${pageContext.request.contextPath}/resources/js/domitory_detail.js"></script>
</body>
</html>